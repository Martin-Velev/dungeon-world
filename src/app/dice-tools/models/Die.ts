import { Faces } from './Faces';

export default class Die {
  face: Faces;
  value: number;

  constructor(face: Faces, value?: number) {
    this.face = face;
    // set max by default
    this.value = value ? value : face;
  }

  roll() {
    this.value = Math.floor(Math.random() * this.face + 1);
  }
}
