import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { DiceRollerPage } from './pages/dice-roller-page/dice-roller.page';
import { DiceHolderComponent } from './components/dice-holder/dice-holder.component';

const routes: Routes = [
  {
    path: '',
    component: DiceRollerPage,
  },
];

@NgModule({
  imports: [CommonModule, FormsModule, IonicModule, RouterModule.forChild(routes)],
  declarations: [DiceRollerPage, DiceHolderComponent],
})
export class DiceRollerPageModule {}
