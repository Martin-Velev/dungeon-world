import { Component, OnInit } from '@angular/core';
import Die from '../../models/Die';
import { Faces } from '../../models/Faces';

const DIE_FACES = [4, 6, 8, 10, 12, 20];

@Component({
  selector: 'app-dice-roller',
  templateUrl: './dice-roller.page.html',
  styleUrls: ['./dice-roller.page.scss'],
})
export class DiceRollerPage implements OnInit {
  selectedDice: Die[];
  diceSet: Die[];

  constructor() {}

  ngOnInit() {
    this.selectedDice = [];
    this.diceSet = DIE_FACES.map(dieFace => new Die(dieFace));
  }

  roll() {
  }

  onDiceSelected(die: Die) {
    this.selectedDice.push(die);
    console.log(this.selectedDice)
  }
}
