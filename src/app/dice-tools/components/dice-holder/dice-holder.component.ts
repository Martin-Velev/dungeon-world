import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { Faces } from '../../models/Faces';
import Die from '../../models/Die';


@Component({
  selector: 'app-dice-holder',
  templateUrl: './dice-holder.component.html',
  styleUrls: ['./dice-holder.component.scss'],
})
export class DiceHolderComponent implements OnInit {
  @Output() diceSelected = new EventEmitter<Die>();
  @Input() dice: Die[]

  constructor() {}

  ngOnInit() {}

  onDiceClicked(die: Die) {
    this.diceSelected.emit(die);
  }
}
